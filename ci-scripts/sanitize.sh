#!/bin/bash
#Author: Joakim Sørensen @ludeeus
#
# usage: sanitize.sh [search] [replace] [dir]
# example: bash sanitize.sh Doffen Person1 /publish

search=$1
replace=$2

files=$(find $3 -type f)

for file in $files; do
    if [[ ${file} != *".git"* ]];then
    sed -i "s/$search/$replace/g" "$file"
    fi
done

echo "sanitize.sh finished.."